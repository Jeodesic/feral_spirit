from random import random

from ability import Ability
from character import Character
from status import Status
from target import Target

lightning_bolt = "Lightning Bolt"
lava_burst = "Lava Burst"
flame_shock = "Flame Shock"
frost_shock = "Frost Shock"
ele_shaman_priority_queue = [flame_shock, lava_burst, lightning_bolt, frost_shock]  # TODO: Priority class with use rules


def basic_damage_formula(base_damage, crit_multiplier=2.0, crit_function=lambda c, t: random() < c.crit_chance,
                         variance=0.05):
    return lambda c, t: int(
        (2 * variance * random() + 1 - variance)
        * (c.versatility + 1)
        * (crit_multiplier * base_damage if crit_function(c, t) else base_damage))


ele_shaman_ability_map = {
    lightning_bolt: Ability(lightning_bolt, 1.5, basic_damage_formula(0.95, 2.25), 0),
    lava_burst: Ability(lava_burst, 2, basic_damage_formula(0.972, 2.25, lambda c, t: t.has_status(flame_shock) or random() < c.crit_chance), 8),
    frost_shock: Ability(frost_shock, 1.5, basic_damage_formula(0.45, 2.25), 10),
    flame_shock: Ability(flame_shock, 1.5, basic_damage_formula(0.195, 2.25), 15,
                         Status(flame_shock, 20, 2, basic_damage_formula(1.044, 2.25), None))}


class Simulation:

    def __init__(self, character, fight_time):
        self.fight_time = fight_time
        self.character = character
        self.time = 0
        self.next_cast = 0
        self.next_tick = 0
        self.target = Target()

    def run(self):
        while self.time < self.fight_time:
            self.target.status_ticks(self.time, self.character)
            if self.time == self.next_cast:
                self.next_cast = self.character.cast_next_ability(self.time, self.target)
            self.next_tick = self.target.next_tick
            self.time = min(self.next_cast, self.next_tick)
        print("Total dps: " + str(self.target.damage_taken / self.fight_time))


sim = Simulation(Character(ele_shaman_ability_map, ele_shaman_priority_queue), 300)
sim.run()
