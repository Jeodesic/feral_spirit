import math


class Target:
    def __init__(self):
        self.damage_taken = 0
        self.statuses = dict()
        self.next_tick = 0

    def apply_status(self, status):
        if status is not None:
            if status.name in self.statuses:
                self.statuses[status.name] = status.update(self.statuses[status.name])
            else:
                self.statuses[status.name] = status
        self.next_tick = self.__get_next_tick_time__()

    def __get_next_tick_time__(self):
        return math.inf if len(self.statuses) == 0 else min(self.statuses[status].next_tick for status in self.statuses)

    def apply_damage(self, damage):
        self.damage_taken += damage

    def expire_statuses(self, current_time):
        self.statuses = {
            status_name: self.statuses[status_name] for status_name in self.statuses if current_time < self.statuses[status_name].expiry_time()}

    def status_ticks(self, current_time, character):
        self.expire_statuses(current_time)
        for status_name in self.statuses:
            status = self.statuses[status_name]
            if status.next_tick == current_time:
                status.tick(self, character)
        self.next_tick = self.__get_next_tick_time__()

    def has_status(self, status_name):
        return status_name in self.statuses