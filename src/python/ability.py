class Ability:
    def __init__(self, name, cast_time, damage_function, cooldown, status=None):
        self.name = name
        self.damage_function = damage_function
        self.cast_time = cast_time
        self.cooldown = cooldown
        self.time_of_next_cast = 0
        self.status = status

    def cast(self, current_time, target, character):
        self.time_of_next_cast = current_time + (self.cast_time * (1 - character.haste)) + self.cooldown
        damage = self.damage_function(character, target)  # TODO: Calculate damage correctly
        target.apply_damage(damage)
        target.apply_status(self.get_status_effect(current_time, character))
        # TODO: Procs
        print("Cast " + self.name + " for " + str(damage) + " damage.")

    def is_off_cooldown(self, current_time):
        return self.time_of_next_cast <= current_time

    def get_status_effect(self, current_time, character):
        return None if self.status is None else self.status.at_time(current_time, character)
