class Status:
    def __init__(self, name, dot_length, tick_length, tick_damage, character, start_time=0):
        self.name = name
        self.dot_length = dot_length
        self.tick_length = tick_length
        self.tick_damage = tick_damage
        self.start_time = start_time
        self.next_tick = 0 if character is None else start_time + tick_length * (1 - character.haste)

    def at_time(self, current_time, character):
        return Status(self.name, self.dot_length, self.tick_length, self.tick_damage, character, current_time)

    def tick(self, target, character):
        damage = self.tick_damage(character, target)
        target.apply_damage(damage)
        print(self.name + " ticked for " + str(damage) + " damage")
        self.next_tick += self.tick_length * (1 - character.haste)
        # TODO: Procs

    def expiry_time(self):
        return self.start_time + self.dot_length

    def update(self, old_status):
        # TODO: Pandemic
        return self
