class Character:
    def __init__(self, abilities, priority_queue):
        self.buffs = dict()
        self.ability_map = abilities
        self.priority_queue = priority_queue
        self.crit_chance = 0.15
        self.mastery = 0.6
        self.haste = 0.11
        self.versatility = 0.03

    def cast_next_ability(self, current_time, target):
        for ability_name in self.priority_queue:
            ability = self.ability_map[ability_name]
            if ability.is_off_cooldown(current_time):
                ability.cast(current_time, target, self)
                return current_time + ability.cast_time
